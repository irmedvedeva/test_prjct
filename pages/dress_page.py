# search 'dress', verify if any item is found
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from pages.base_page import BasePage
from locators.dress_page_locators import DressPageLocators


class DressPage(BasePage):
    dress_number_selector = DressPageLocators.dress_number_selector
    dress_results_selector = DressPageLocators.dress_results_selector

    def dress(self):
        WebDriverWait(self.driver, 10).until(
            EC.visibility_of_element_located(self.dress_results_selector))
        # added:show how many items have been found: 7 results have been found.
        self.driver.find_element(*self.dress_number_selector)
