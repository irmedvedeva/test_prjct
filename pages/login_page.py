from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from pages.base_page import BasePage
from locators.login_page_locators import LoginPageLocators
from data.user_data import UserData


class LoginPage(BasePage):

    email_selector = LoginPageLocators.email_selector
    password_selector = LoginPageLocators.password_selector
    sign_in_button_selector = LoginPageLocators.sign_in_button_selector

    def login(self):
        mail = UserData.mail
        passwd = UserData.passwd

        WebDriverWait(self.driver, 10).until(EC.visibility_of_element_located(
            self.email_selector)).send_keys(mail)
        self.driver.find_element(*self.password_selector).send_keys(passwd)
        self.driver.find_element(*self.sign_in_button_selector).click()
