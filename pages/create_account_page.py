from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from pages.base_page import BasePage
import time
from locators.create_acc_locators import CreateAccLocators
from data.create_acc_data import CreateAccData


class NewAccountPage(BasePage):

    mrs_selector = CreateAccLocators.mrs_selector
    first_name_selector = CreateAccLocators.first_name_selector
    last_name_selector = CreateAccLocators.last_name_selector
    password_selector = CreateAccLocators.password_selector
    address_selector = CreateAccLocators.address_selector
    city_selector = CreateAccLocators.city_selector
    state_selector = CreateAccLocators.state_selector
    exact_state_selector = CreateAccLocators.exact_state_selector
    zip_code_selector = CreateAccLocators.zip_code_selector
    country_selector = CreateAccLocators.country_selector
    exact_country_selector = CreateAccLocators.exact_country_selector
    phone_selector = CreateAccLocators.phone_selector
    alias_selector = CreateAccLocators.alias_selector
    button_register_selector = CreateAccLocators.button_register_selector

    def create_account(self):
        first_name = CreateAccData.first_name
        last_name = CreateAccData.last_name
        password = CreateAccData.password
        address = CreateAccData.address
        city = CreateAccData.city
        zip_code = CreateAccData.zip_code
        phone = CreateAccData.phone
        alias = CreateAccData.alias

        WebDriverWait(self.driver, 10).until(
            EC.visibility_of_element_located(self.mrs_selector)).click()
        self.driver.find_element(*self.first_name_selector).send_keys(
            first_name)
        self.driver.find_element(*self.last_name_selector).send_keys(last_name)
        self.driver.find_element(*self.password_selector).send_keys(password)
        self.driver.find_element(*self.address_selector).send_keys(address)
        self.driver.find_element(*self.city_selector).send_keys()

        self.driver.find_element(*self.state_selector).click()
        WebDriverWait(self.driver, 10).until(EC.visibility_of_element_located(
            self.exact_state_selector)).click()

        self.driver.find_element(*self.zip_code_selector).send_keys(zip_code)
        time.sleep(3)
        self.driver.find_element(*self.country_selector).click()
        time.sleep(3)

        WebDriverWait(self.driver, 10).until(EC.visibility_of_element_located(
            self.exact_country_selector)).click()
        self.driver.find_element(*self.phone_selector).send_keys(phone)

        self.driver.find_element(*self.alias_selector).send_keys(alias)
        self.driver.find_element(*self.button_register_selector).click()






