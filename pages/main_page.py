from selenium.webdriver.common.by import By
from pages.base_page import BasePage
from locators.main_page_locators import MainPageLocators
from data.main_data import MainData


class MainPage(BasePage):
    login_selector = MainPageLocators.login_selector
    search_box_selector = MainPageLocators.search_box_selector
    submit_search_selector = MainPageLocators.submit_search_selector

    def go_to_login(self):
        self.driver.find_element(*self.login_selector).click()

    def search_dress(self):
        item = MainData.item
        self.driver.find_element(*self.search_box_selector).send_keys(item)
        self.driver.find_element(*self.submit_search_selector).click()
