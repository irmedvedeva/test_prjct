from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from pages.base_page import BasePage
import time
from locators.add_to_cart_page_locators import AddToCartLocators

class AddToCart(BasePage):

    def add_to_cart(self):
        but_add_to_cart = AddToCartLocators.but_add_to_cart
        item_added = AddToCartLocators.item_added
        title = AddToCartLocators.title
        quantity = AddToCartLocators.quantity
        try:
            self.driver.find_element(but_add_to_cart).click()
            time.sleep(3)
            assert self.driver.find_element(item_added) is True
            def go_to_cart(self):
                try:
                    self.driver.find_element(item_added).click()
                    time.sleep(3)
                    assert title in self.driver.title
                    def cart_quantity():
                        self.driver.find_element(quantity)
                finally:
                    return True
        finally:
            self.driver.quit()    
