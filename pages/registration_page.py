from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from pages.base_page import BasePage
from locators.login_page_locators import LoginPageLocators
from data.user_data import UserData


class RegistrationPage(BasePage):
    new_email_selector = LoginPageLocators.new_email_selector
    create_account_selector = LoginPageLocators.create_account_selector

    def registration(self):
        new_mail = UserData.new_mail
        WebDriverWait(self.driver, 10).until(EC.visibility_of_element_located(
            self.new_email_selector)).send_keys(new_mail)
        self.driver.find_element(*self.create_account_selector).click()
