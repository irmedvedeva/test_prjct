from selenium.webdriver.common.by import By


class AddToCartLocators:
    but_add_to_cart = (By.LINK_TEXT, 'Add to cart')
    item_added = (By.LINK_TEXT, 'Proceed to checkout')
    title = (By.LINK_TEXT, "Oder")
    quantity = (By.CLASS_NAME, "cart_quantity_input form-control grey")
