from selenium.webdriver.common.by import By


class DressPageLocators:
    dress_number_selector = (By.CSS_SELECTOR, ".heading-counter")
    dress_results_selector = (By.XPATH, "//div[@class='product-container']")