from selenium.webdriver.common.by import By


class CreateAccLocators:
    mrs_selector = (By.ID, "id_gender2")
    first_name_selector = (By.CSS_SELECTOR, "#customer_firstname")
    last_name_selector = (By.ID, "customer_lastname")
    password_selector = (By.ID, "passwd")
    address_selector = (By.CSS_SELECTOR, "#address1")
    city_selector = (By.NAME, "city")
    state_selector = (By.CSS_SELECTOR, "#id_state")
    exact_state_selector = (By.XPATH, "//option[contains(text(),'California')]")
    zip_code_selector = (By.ID, "postcode")
    country_selector = (By.ID, "uniform-id_country")
    exact_country_selector = (By.XPATH, "//option[contains(text(),'United States')]")
    phone_selector = (By.CSS_SELECTOR, "#phone_mobile")
    alias_selector = (By.CSS_SELECTOR, ".form-control")
    button_register_selector = (By.NAME, "submitAccount")