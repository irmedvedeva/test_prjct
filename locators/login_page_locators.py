from selenium.webdriver.common.by import By


class LoginPageLocators:
    # login
    email_selector = (By.ID, "email")
    password_selector = (By.ID, "passwd")
    sign_in_button_selector = (By.ID, "SubmitLogin")

    # registration
    new_email_selector = (By.ID, "email_create")
    create_account_selector = (By.ID, "SubmitCreate")