from selenium.webdriver.common.by import By


class MainPageLocators:
    LOGIN_LINK = (By.CLASS_NAME, '.login')
    login_selector = (By.CLASS_NAME, "login")
    search_box_selector = (By.ID, "search_query_top")
    submit_search_selector = (By.NAME, "submit_search")

